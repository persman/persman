package persdev.persman;

import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import android.app.Service;
import android.view.WindowManager;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import persdev.persman.activities.AlarmClockActivity;
import persdev.persman.database.DatabaseManagement;
import persdev.persman.models.TableEvents;

/**
 * Created by lukasz_tyburski on 2016-03-18.
 */
public class CheckEventService extends Service{
       private Integer intervalService= 10000;
       private DatabaseManagement db = new DatabaseManagement(this);
       ExecutorService executor = Executors.newFixedThreadPool(1);

        @Override
        public int onStartCommand(Intent intent, int flags, int startId) {
            executor.submit(new CheckEventThread());
            return START_NOT_STICKY;
        }

        @Override
        public void onDestroy() {
            executor.shutdownNow();
            this.stopSelf();
        }

        @Override
        public IBinder onBind(Intent intent) {
            return null;
        }

         public class CheckEventThread implements Runnable{

            @Override
            public void run() {
                //db.addEventToDatabase(new TableEvents("Event", "2016-03-19 02:37:01", "21-03-16", 1, 57.2345f, 23.1234f, 10, "default"));
                while(true) {
                    try {
                        Thread.sleep(intervalService);
                        TableEvents event = db.selectEventForRemind();
                        if (event.getId() != 0 ) {
                            Log.d("lala", "Kategoria eventu do wyswietlenia " + event.getCategory());
                            /**
                             * Ustawiamy flagę enable na 0 aby po raz kolejny nie został wyświetlony ten sam event
                             */
                            event.setFlag(0);
                            db.modifyEvent(event);

                            Intent in = new Intent(getApplicationContext(), AlarmClockActivity.class);
                            in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            in.putExtra("event", event);
                            startActivity(in);

                        } else {
                            Log.d("lala", "Nic nie ma zadnego eventu do wyswietlenia");
                        }

                        if (Thread.interrupted())
                            throw new InterruptedException();
                    } catch (InterruptedException e) {
                        return;
                    }
                }
            }
        }
}
