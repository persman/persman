package persdev.persman.activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import persdev.persman.R;
import persdev.persman.weather.Channel;
import persdev.persman.weather.Condition;
import persdev.persman.weatherService.WeatherServiceCallback;
import persdev.persman.weatherService.YahooWeatherService;

public class WeatherActivity extends AppCompatActivity implements WeatherServiceCallback {
    private YahooWeatherService serviceOfWeather;
    private Condition condition;
    private String description;
    private String temperatureString;
    private ImageView weatherImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        serviceOfWeather = new YahooWeatherService(this);
        setContentView(R.layout.activity_weather);
        weatherImage = (ImageView) findViewById(R.id.weatherImage);
        SharedPreferences sharedPref = this.getPreferences(Context.MODE_PRIVATE);
        String townText = sharedPref.getString("townWeather", "");
        if(!townText.equals("")){
            EditText townEditText =  (EditText) findViewById(R.id.townText);
            townEditText.setText(townText);
            serviceOfWeather.getWeatherDescription(townText, getApplicationContext());
        }

    }

    @Override
    public void serviceSuccess(Channel channel) {
        condition = channel.getItem().getCondition();
        description = condition.getDescription();
        temperatureString = getString(R.string.temperature_output, condition.getTemperature(), channel.getUnits().getTemperature());
        Integer temperature = condition.getTemperature();
        if(description.toLowerCase().contains("rain")){
            weatherImage.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.deszcz));
        } else {
            if(temperature < -10){
                weatherImage.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.zimno));
            }
            if(temperature >=-10 &&temperature <0 ){
                weatherImage.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.zimnomniej));
            }
            if(temperature >=0 && temperature < 15 ){
                weatherImage.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.persman));
            }
            if(temperature >= 15 && temperature < 25){
                weatherImage.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.cieplo));
            }
            if(temperature >= 25){
                weatherImage.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.bardzocieplo));
            }
        }
        ((TextView)findViewById(R.id.temperatureTextView)).setText(temperatureString);
        weatherImage.invalidate();
    }

    @Override
    public void serviceFailure(Exception exception) {
        Toast.makeText(this, exception.getMessage(), Toast.LENGTH_LONG).show();
    }

    public void okButtonClick(View view) {
        EditText townText =  (EditText) findViewById(R.id.townText);
        if (!townText.getText().toString().equals("")){
            SharedPreferences sharedPref = this.getPreferences(Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString("townWeather", townText.getText().toString());
            editor.commit();
            serviceOfWeather.getWeatherDescription(townText.getText().toString(), getApplicationContext());
        }

    }
}
