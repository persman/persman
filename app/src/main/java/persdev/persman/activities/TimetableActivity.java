package persdev.persman.activities;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Formattable;
import java.util.List;

import persdev.persman.R;
import persdev.persman.database.DatabaseManagement;
import persdev.persman.list.CustomList;
import persdev.persman.models.TableEvents;

public class TimetableActivity extends AppCompatActivity {
    private Button back;
    private Button next;
    private ListView listView;
    private CustomList adapter;
    private TextView date;
    private Calendar calendar;
    List<TableEvents> events;
    private Date sysdate;
    private DatabaseManagement db;
    ImageView deleteButton;
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timetable);

        back = (Button) findViewById(R.id.back);
        next = (Button) findViewById(R.id.next);
        date = (TextView) findViewById(R.id.date);
        deleteButton = (ImageView) findViewById(R.id.delete);

        db = new DatabaseManagement(this);

        listView = (ListView) findViewById(R.id.list);


        sysdate = db.getSysdate();
        calendar = Calendar.getInstance();
        calendar.setTime(sysdate);
        String s = sdf.format(sysdate);
        date.setText(s);

        List<TableEvents> events = db.selectDayEvent(s);
        listView.setAdapter(new CustomList(this, events,s));
//        Log.d("event", events.get(0).getDateFrom());

    }

    public void runBack(View view) {
        calendar = Calendar.getInstance();
        calendar.setTime(sysdate);
        calendar.set(Calendar.DAY_OF_MONTH, calendar.get(Calendar.DAY_OF_MONTH) - 1);
        sysdate = calendar.getTime();
        String s = sdf.format(sysdate);
        date.setText(s);

        events = db.selectDayEvent(s);
       // List<TableEvents> events = db.selectDayEvent(s + " 00:00:00", s + " 23:59:59");
        listView.setAdapter(new CustomList(this, events,s));
    }

    public void chooseDate(View view) {

    }

    public void runThrough(View view) {
        calendar = Calendar.getInstance();
        calendar.setTime(sysdate);
        calendar.set(Calendar.DAY_OF_MONTH, calendar.get(Calendar.DAY_OF_MONTH) + 1);
        sysdate = calendar.getTime();
        String s = sdf.format(sysdate);
        date.setText(s);
        final List<TableEvents> events = db.selectDayEvent(s);
        listView.setAdapter(new CustomList(this, events,s));

    }

    public void addToNewEventActivity(View view) {
        Intent intent = new Intent(this, AddNewEventActivity.class);
        startActivity(intent);
    }
}
