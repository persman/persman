package persdev.persman.activities;
import android.content.Intent;
import android.os.BatteryManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import persdev.persman.CheckEventService;
import persdev.persman.R;
import persdev.persman.weather.Channel;
import persdev.persman.weather.Condition;
import persdev.persman.weatherService.WeatherServiceCallback;
import persdev.persman.weatherService.YahooWeatherService;


public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.xml.fadein, R.xml.fadeout);
        setContentView(R.layout.activity_main);
        startFollow(CheckEventService.class);
    }

    public void alarmClockButtonClick(View view) {
        Intent intent = new Intent(this, AlarmClockActivity.class);
        startActivity(intent);
    }

    public void timetableButtonClick(View view) {
        Intent intent = new Intent(this, TimetableActivity.class);
        startActivity(intent);
    }

    public void weatherButtonClick(View view) {
        Intent intent = new Intent(this, WeatherActivity.class);
        startActivity(intent);
    }

    public void settingsButtonClick(View view) {
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);
    }

    public void exitButtonClick(View view) {
        stopFollow(CheckEventService.class);
        android.os.Process.killProcess(android.os.Process.myPid());
    }

    public void startFollow(Class<?> serviceClass){
        Intent intent  = new Intent(getApplicationContext(), serviceClass);
        startService(intent);
    }

    public void stopFollow(Class<?> serviceClass){
        Intent intent = new Intent(getApplicationContext(), serviceClass);
        stopService(intent);
    }

}