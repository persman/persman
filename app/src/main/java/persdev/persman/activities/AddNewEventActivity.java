package persdev.persman.activities;
import android.app.DatePickerDialog;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import persdev.persman.R;
import persdev.persman.database.DatabaseManagement;
import persdev.persman.models.TableEvents;

public class AddNewEventActivity extends Activity{
    private String[] category = { "Spotkanie", "Praca", "Studia", "Szkoła",
            "Zakupy", "Do zrobienia", "Wyjazd" };
    private Spinner spinnerCategory;
    private SeekBar interval;
    private EditText titleEvent;
    private TextView dateFrom;
    private TextView dateTo;
    private Button dateFromButton;
    private Button dateFromButton2;
    private Button dateToButton;
    private Button dateToButton2;
    private CheckBox checkInterval;
    private String strCategory;
    private TableEvents tableEvents = new TableEvents();
    private TextView intervalMinute;

    private Calendar calDateFrom=Calendar.getInstance();
    private Calendar calDateFrom2=Calendar.getInstance();
    private Calendar calDateTo=Calendar.getInstance();
    private Calendar calDateTo2=Calendar.getInstance();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_event);
        spinnerCategory = (Spinner) findViewById(R.id.category);
        interval = (SeekBar) findViewById(R.id.intervalSeekBar);
        titleEvent = (EditText) findViewById(R.id.titleEvent);
        intervalMinute = (TextView) findViewById(R.id.intervalMinute);
        dateFrom  = (TextView) findViewById(R.id.dateFrom);
        dateTo  = (TextView) findViewById(R.id.dateto);
        dateFromButton  = (Button) findViewById(R.id.dateFromButton);
        dateFromButton2  = (Button) findViewById(R.id.dateFromButton2);
        dateToButton  = (Button) findViewById(R.id.dateToButton);
        dateToButton2  = (Button) findViewById(R.id.dateToButton2);
        checkInterval  = (CheckBox) findViewById(R.id.intervalCheckbox);

        interval.setEnabled(false);

        ArrayAdapter<String> adapter_category = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, category);
        adapter_category.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerCategory.setAdapter(adapter_category);

        spinnerCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                spinnerCategory.setSelection(position);
                strCategory = (String) spinnerCategory.getSelectedItem();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        interval.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                intervalMinute.setText(progress + 15 + " min");
            }
        });

        dateFromButton2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                new DatePickerDialog(AddNewEventActivity.this, pickerFrom2, calDateFrom2.get(Calendar.YEAR), calDateFrom.get(Calendar.MONTH), calDateFrom2.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        dateFromButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                new DatePickerDialog(AddNewEventActivity.this, pickerFrom, calDateFrom.get(Calendar.YEAR), calDateFrom.get(Calendar.MONTH), calDateFrom.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        dateToButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                new TimePickerDialog(AddNewEventActivity.this, pickerTo, calDateFrom.get(Calendar.HOUR_OF_DAY), calDateFrom.get(Calendar.MINUTE),true).show();
            }
        });

        dateToButton2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                new TimePickerDialog(AddNewEventActivity.this, pickerTo2, calDateFrom.get(Calendar.HOUR_OF_DAY), calDateFrom.get(Calendar.MINUTE),true).show();
            }
        });



        checkInterval.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    interval.setEnabled(true);
                    intervalMinute.setText(15 + " min");
                } else {
                    interval.setEnabled(false);
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_new_event, menu);
        return true;
    }

    public void saveEvent(View view) {
        tableEvents.setName(titleEvent.getText().toString());
        tableEvents.setDateFrom(dateFromButton.getText() + " " + dateToButton.getText());
        tableEvents.setDateTo(dateFromButton2.getText() + " " + dateToButton2.getText());

        tableEvents.setFlag(1);
        tableEvents.setDateAlarm(interval.getProgress());
        if ( !checkInterval.isChecked() ){
            tableEvents.setDateAlarm(interval.getProgress());
        }
        tableEvents.setCategory(strCategory);
     //   DatabaseManagement db = new DatabaseManagement(this);
       // db.addEventToDatabase(tableEvents);
        Intent intent = new Intent(this, MapsAcitivity.class);
        intent.putExtra("event", tableEvents);
        startActivity(intent);
    }

    DatePickerDialog.OnDateSetListener pickerFrom = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month , int day) {
            String day2;

            if ( day < 9 ) {
                day2 = "0"+day;
            } else {
                day2 = String.valueOf(day);
            }

            String month2;
            if ( month < 9 ) {
                month2 = "0"+String.valueOf(month+1);
            } else {
                month2 = String.valueOf(month+1);
            }
            dateFromButton.setText(year+"-"+month2+"-"+day2);
        }
    };

    DatePickerDialog.OnDateSetListener pickerFrom2 = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month , int day) {
            String day2;

            if ( day < 9 ) {
                day2 = "0"+day;
            } else {
                day2 = String.valueOf(day);
            }

            String month2;
            if ( month < 9 ) {
                month2 = "0"+String.valueOf(month+1);
            } else {
                month2 = String.valueOf(month+1);
            }
            dateFromButton2.setText(year+"-"+month2+"-"+day2);
        }
    };

TimePickerDialog.OnTimeSetListener pickerTo = new TimePickerDialog.OnTimeSetListener() {

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        dateToButton.setText(hourOfDay + ":" + minute+":00");
    }
};

    TimePickerDialog.OnTimeSetListener pickerTo2 = new TimePickerDialog.OnTimeSetListener() {

        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            dateToButton2.setText(hourOfDay + ":" + minute+":00");
        }
    };


}
