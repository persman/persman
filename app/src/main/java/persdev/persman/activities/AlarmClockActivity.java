package persdev.persman.activities;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.media.MediaPlayer;
import android.os.BatteryManager;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import persdev.persman.R;
import persdev.persman.database.DatabaseManagement;
import persdev.persman.models.Point;
import persdev.persman.models.TableEvents;
import persdev.persman.weather.Channel;
import persdev.persman.weather.Condition;
import persdev.persman.weatherService.WeatherServiceCallback;
import persdev.persman.weatherService.YahooWeatherService;

public class AlarmClockActivity extends AppCompatActivity implements WeatherServiceCallback {

    private YahooWeatherService serviceOfWeather;
    private Condition condition;
    private String description;
    private String temperature;

    private Bundle extras;
    private TableEvents tableEvent;
    private Intent intent;

    private DatabaseManagement db;

    MediaPlayer mp;
    Vibrator v;

    String filePath;
    boolean vibrationState;
    Integer volume;

    TextView naglowekTextView;
    TextView tytulTextView;
    TextView godzinaTextView;
    TextView miejsceTextView;
    ListView informacjeListView;

    Integer level;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm_clock);
        mp = new MediaPlayer();
        SharedPreferences sharedPref = this.getPreferences(Context.MODE_PRIVATE);
        filePath = sharedPref.getString("dzwiekPath", "");
        vibrationState = sharedPref.getBoolean("vibrationState", true);
        volume = sharedPref.getInt("volume", 50);
        if(!filePath.equals("")) {
            try {
                mp.setVolume((float)volume/100,(float)volume/100);
                mp.setDataSource(filePath);
                mp.setLooping(true);
                mp.start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        long[] pattern = {0, 1000};
        v.vibrate(pattern, 0);


        extras = getIntent().getExtras();
        intent = getIntent();

        if (extras != null) {
            tableEvent = (TableEvents) intent.getSerializableExtra("event");
        }



        final Window win= getWindow();
        win.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        win.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);

        tytulTextView = (TextView)findViewById(R.id.tytulTextView);
        godzinaTextView = (TextView)findViewById(R.id.godzinaTextView);
        miejsceTextView = (TextView)findViewById(R.id.miejsceTextView);
        informacjeListView = (ListView)findViewById(R.id.informacjeListView);
        //stan baterii
        Intent batteryIntent = registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        level = batteryIntent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);
        //Toast.makeText(this, String.valueOf(level) + "%", Toast.LENGTH_LONG).show();
        serviceOfWeather = new YahooWeatherService(this);
        if(tableEvent!=null){
            serviceOfWeather.getWeatherDescription(tableEvent.getCity(), this);
        }


    }

    @Override
    public void serviceSuccess(Channel channel) {
        condition = channel.getItem().getCondition();
        description = condition.getDescription();
        temperature = getString(R.string.temperature_output, condition.getTemperature(), channel.getUnits().getTemperature());

        if(tableEvent.getName()!=null) tytulTextView.setText(tableEvent.getName());
        else tytulTextView.setText("");
        String[] tmp = tableEvent.getDateFrom().split(" ");
        String[] hour = tmp[1].split(":");
        godzinaTextView.setText(hour[0] + ":" + hour[1]);
        if(tableEvent.getCity()!=null) miejsceTextView.setText(tableEvent.getCity());
        else miejsceTextView.setText(tableEvent.getCity());
        List<String> listInfos = new ArrayList<>();


        if(condition.getDescription().contains("rain")){
            listInfos.add("Mozliwe opady deszczu, zabierz parasolke");
        }
        if(level<50 && level >=25){
            listInfos.add("Bateria ponizej 50%, radze podladowac telefon");
        } else if(level <25){
            listInfos.add("Bardzo niski stan baterii, naladuj telefon");
        }
        if(condition.getTemperature() < 0){
            listInfos.add("Ubierz sie cieplo, w tym miejscu moze byc " + temperature);
        }




        String[] infosForAdapter = listInfos.toArray(new String[0]);


        ArrayAdapter adapter = new ArrayAdapter(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, infosForAdapter);

        // Assign adapter to ListView
        informacjeListView.setAdapter(adapter);
        //Toast.makeText(this, description + " " + temperature, Toast.LENGTH_LONG).show();
    }

    @Override
    public void serviceFailure(Exception exception) {
        Toast.makeText(this, exception.getMessage(), Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onDestroy() {
        mp.stop();
        v.cancel();;
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        mp.stop();
        v.cancel();
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        mp.stop();
        v.cancel();
        super.onBackPressed();
    }
}