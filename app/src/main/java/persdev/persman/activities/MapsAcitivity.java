package persdev.persman.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import persdev.persman.R;
import persdev.persman.database.DatabaseManagement;
import persdev.persman.models.TableEvents;

public class MapsAcitivity extends Activity implements GoogleMap.OnMapLongClickListener, LocationListener {
    private GoogleMap googleMap;
    public float lng;
    public float lat;
    public boolean oneMarker = true;
    private EditText city;
    private Bundle extras;
    private TableEvents tableEvents;
    private Intent intent;
    private DatabaseManagement db;
    public String locationStr;
    public String result = "";
    private LocationManager lm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        city = (EditText) findViewById(R.id.city);
        extras = getIntent().getExtras();
        intent = getIntent();

        lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 10, this);

        LocationManager locationManager = (LocationManager)this.getSystemService(Context.LOCATION_SERVICE);
        List<String> providers = locationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            Location l = locationManager.getLastKnownLocation(provider);
            if (l == null) {
                continue;
            }
            if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                bestLocation = l;
            }
        }

        if (extras != null) {
            tableEvents = (TableEvents) intent.getSerializableExtra("event");
        }

        try {
            if (googleMap == null) {
                googleMap = ((MapFragment) getFragmentManager().
                        findFragmentById(R.id.map)).getMap();
            }
            googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        } catch (Exception e) {
            e.printStackTrace();
        }
        googleMap.setOnMapLongClickListener(this);

        if(bestLocation != null) {
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(bestLocation.getLatitude(),
                    bestLocation.getLongitude()), 10));
        }
    }

    @Override
    public void onMapLongClick(LatLng point) {
        googleMap.clear();
        lat = (float) 0.0;
        lng = (float) 0.0;
        oneMarker = true;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_maps_acitivity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void searchLocation(View view) {
        if (oneMarker) {
            String searchText = city.getText().toString();
            List<Address> addresses = new ArrayList<>();

            String add = "";
            locationStr = city.getText().toString();
            try {
                Geocoder geoCoder = new Geocoder(this, Locale.getDefault());
                addresses = geoCoder.getFromLocationName(locationStr, 1);
                if (addresses != null) {
                    for (Address address : addresses) {
                        for (int i = 0, j = address.getMaxAddressLineIndex(); i <= j; i++) {
                            result += address.getAddressLine(i) + "\n";
                        }

                        geoCoder.getFromLocation(address.getLatitude(), address.getLongitude(), 1);
                        LatLng position = new LatLng(address.getLatitude(), address.getLongitude());
                        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 10));
                       // CameraUpdate cameraPosition = CameraUpdateFactory.newLatLng(position);
                       // googleMap.animateCamera(cameraPosition);
                        lat = (float) position.latitude;
                        lng = (float) position.longitude;

                        MarkerOptions marker = new MarkerOptions();
                        marker.position(position);
                        googleMap.addMarker(marker);
                        oneMarker = false;
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void saveLoc(View view) {
        if(lat != 0.0 && lng != 0.0) {
            tableEvents.setLatitude(lat);
            Log.d("lat save loc", "" + tableEvents.getLatitude());
            tableEvents.setLongitude(lng);
            tableEvents.setCity(locationStr);
            db = new DatabaseManagement(this);
            db.addEventToDatabase(tableEvents);
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
