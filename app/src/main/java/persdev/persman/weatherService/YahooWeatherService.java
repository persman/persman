package persdev.persman.weatherService;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.internal.widget.TintContextWrapper;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import persdev.persman.weather.Channel;

/**
 * Created by shoorie on 18.03.16.
 */
public class YahooWeatherService  {

    private Exception error;
    private String loc;
    private WeatherServiceCallback weatherServiceCallback;
    private String temperatureUnit = "C";


    public YahooWeatherService(WeatherServiceCallback weatherServiceCallback) {
        this.weatherServiceCallback = weatherServiceCallback;
    }

    public void getWeatherDescription(String location, final Context context){

        this.loc = location;

        new AsyncTask<String, Void, Channel>() {
            @Override
            protected Channel doInBackground(String... locations) {
                String location = locations[0];

                String unit = getTemperatureUnit().equalsIgnoreCase("f") ? "f" : "c";

                String YQL = String.format("select * from weather.forecast where woeid in" +
                        " (select woeid from geo.places(1) where text=\"%s\") and u='" + unit + "'", location);
                String endpoint = String.format("https://query.yahooapis.com/v1/public/yql?q=%s&format=json", Uri.encode(YQL));
                try {
                    URL url = new URL(endpoint);
                    URLConnection connection = url.openConnection();
                    InputStream inputStream = connection.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

                    StringBuilder result = new StringBuilder();
                    String line;

                    while((line = reader.readLine())  != null) {
                        result.append(line);
                    }

                    JSONObject obj = new JSONObject(result.toString());
                    Log.d("tag", result.toString());
                    JSONObject queryResult = obj.optJSONObject("query");

                    int count = queryResult.getInt("count");
                    if(count == 0) Toast.makeText(context, "No weather information", Toast.LENGTH_LONG).show();

                    Channel channel = new Channel();
                    JSONObject channelJSON = queryResult.optJSONObject("results").optJSONObject("channel");
                    channel.populate(channelJSON);

                    return channel;

                } catch (Exception e) {
                    error = e;
                    Log.d("Exception in getWeatherDescription method", String.valueOf(error));
                }
                return null;
            }

            @Override
            protected void onPostExecute(Channel channel) {

                if (channel == null && error != null) {
                    weatherServiceCallback.serviceFailure(error);
                } else {
                    weatherServiceCallback.serviceSuccess(channel);
                }

            }
        }.execute(location);
        }

    public String getTemperatureUnit() {
        return temperatureUnit;
    }

    public void setTemperatureUnit(String temperatureUnit) {
        this.temperatureUnit = temperatureUnit;
    }
}
