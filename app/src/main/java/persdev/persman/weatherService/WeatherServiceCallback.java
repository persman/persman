package persdev.persman.weatherService;

import persdev.persman.weather.Channel;

/**
 * Created by shoorie on 18.03.16.
 */
public interface WeatherServiceCallback {
    void serviceSuccess(Channel channel);
    void serviceFailure(Exception exception);
}
