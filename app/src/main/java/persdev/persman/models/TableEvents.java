package persdev.persman.models;

import android.app.usage.UsageEvents;

import java.io.Serializable;

/**
 * Created by lukasz_tyburski on 2016-03-18.
 */
public class TableEvents implements Serializable{
    private int id;
    private String name;
    private String dateFrom;
    private String dateTo;
    private Integer flag;
    private Float latitude;
    private Float longitude;
    private String city;
    private int dateAlarm;
    private String category;

    public TableEvents() {

    }

    public TableEvents(String name, String dateFrom, String dateTo, Integer flag, Float latitude, Float longitude, int dateAlarm, String category) {
        this.name = name;
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
        this.flag = flag;
        this.latitude = latitude;
        this.longitude = longitude;
        this.dateAlarm = dateAlarm;
        this.category = category;
    }

    public TableEvents(String name, String dateFrom, String dateTo, Integer flag,
                       Float latitude, Float longitude, int dateAlarm, String category, String city) {
        this.name = name;
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
        this.flag = flag;
        this.latitude = latitude;
        this.longitude = longitude;
        this.dateAlarm = dateAlarm;
        this.category = category;
        this.city = city;

    }

    public TableEvents(String name, String dateFrom, String dateTo, Integer flag, Integer dateAlarm, String category) {
        this.name = name;
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
        this.flag = flag;
        this.dateAlarm = dateAlarm;
        this.category = category;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(String dateFrom) {
        this.dateFrom = dateFrom;
    }

    public String getDateTo() {
        return dateTo;
    }

    public void setDateTo(String dateTo) {
        this.dateTo = dateTo;
    }

    public Float getLatitude() {
        return latitude;
    }

    public void setLatitude(Float latitude) {
        this.latitude = latitude;
    }

    public Integer getFlag() {
        return flag;
    }

    public void setFlag(Integer flag) {
        this.flag = flag;
    }

    public Float getLongitude() {
        return longitude;
    }

    public void setLongitude(Float longitude) {
        this.longitude = longitude;
    }

    public int getDateAlarm() {
        return dateAlarm;
    }

    public void setDateAlarm(int dateAlarm) {
        this.dateAlarm = dateAlarm;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}