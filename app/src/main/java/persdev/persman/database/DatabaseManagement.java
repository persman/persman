package persdev.persman.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.format.Time;
import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import persdev.persman.models.TableEvents;

import static persdev.persman.database.EventDatabaseValues.Events.ALARM_DATE;
import static persdev.persman.database.EventDatabaseValues.Events.CATEGORY;
import static persdev.persman.database.EventDatabaseValues.Events.CITY;
import static persdev.persman.database.EventDatabaseValues.Events.DATE_FROM;
import static persdev.persman.database.EventDatabaseValues.Events.DATE_TO;
import static persdev.persman.database.EventDatabaseValues.Events.EVENT_ID;
import static persdev.persman.database.EventDatabaseValues.Events.EVENT_NAME;
import static persdev.persman.database.EventDatabaseValues.Events.FLAG;
import static persdev.persman.database.EventDatabaseValues.Events.LATITUDE;
import static persdev.persman.database.EventDatabaseValues.Events.LONGITUDE;
import static persdev.persman.database.EventDatabaseValues.Events.TABLE_EVENT;
/**
 * Created by shoorie on 18.03.16.
 */
public class DatabaseManagement extends SQLiteOpenHelper {

    private static final String DB_NAME = "persman2.db";
    private static final int DB_VERSION = 2;

    public DatabaseManagement(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + TABLE_EVENT + " ("
                + EVENT_ID + " integer primary key autoincrement,"
                + EVENT_NAME + " text,"
                + DATE_FROM + " text,"
                + DATE_TO + " text,"
                + FLAG + " integer,"
                + LATITUDE + " float,"
                + LONGITUDE + " float,"
                + ALARM_DATE + " integer,"
                + CATEGORY + " text,"
                + CITY + " text);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_EVENT);
    }

    public void addEventToDatabase(TableEvents tableEvents){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(EVENT_NAME, tableEvents.getName());
        values.put(DATE_FROM, tableEvents.getDateFrom());
        values.put(DATE_TO, tableEvents.getDateTo());
        values.put(FLAG, tableEvents.getFlag());
        values.put(LATITUDE, tableEvents.getLatitude());
        values.put(LONGITUDE, tableEvents.getLongitude());
        values.put(ALARM_DATE, tableEvents.getDateAlarm());
        values.put(CATEGORY, tableEvents.getCategory());
        values.put(CITY, tableEvents.getCity());
        db.insert(TABLE_EVENT, null, values);
        db.close();
    }

    public List<TableEvents> selectAllInfoAboutEvent() {
        List<TableEvents> events = new ArrayList<>();
        String eventQuery = "SELECT * FROM " + TABLE_EVENT;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(eventQuery, null);
        if (cursor.moveToFirst()) {
            do {
                TableEvents event = new TableEvents();
                event.setId(cursor.getInt(0));
                event.setName(cursor.getString(1));
                event.setDateFrom(cursor.getString(2));
                event.setDateTo(cursor.getString(3));
                event.setFlag(cursor.getInt(4));
                event.setLatitude(cursor.getFloat(5));
                event.setLongitude(cursor.getFloat(6));
                event.setDateAlarm(cursor.getInt(7));
                event.setCategory(cursor.getString(8));
                event.setCity(cursor.getString(9));

                events.add(event);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return events;
    }

    public TableEvents selectEventForRemind() {
        Date actDate = new Date();
        List<TableEvents> listEvents = selectAllInfoAboutEvent();
        List<TableEvents> listActEvents = new ArrayList<>();
        for(TableEvents event : listEvents) {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            try {
                Date dateFrom = formatter.parse(event.getDateFrom());
                Long dateInterval = (dateFrom.getTime() - actDate.getTime())/(1000*60);
                if ( dateInterval <= event.getDateAlarm() && event.getFlag() == 1) {
                    listActEvents.add(event);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return listActEvents.get(0);

    }

    public void modifyEvent(TableEvents event){

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(EVENT_ID, event.getId());
        values.put(EVENT_NAME, event.getName());
        values.put(DATE_FROM, event.getDateFrom());
        values.put(DATE_TO, event.getDateTo());
        values.put(FLAG, event.getFlag());
        values.put(LATITUDE, event.getLatitude());
        values.put(LONGITUDE, event.getLongitude());
        values.put(ALARM_DATE, event.getDateAlarm());
        values.put(CATEGORY, event.getCategory());
        values.put(CITY, event.getCity());

        db.update(TABLE_EVENT, values, EVENT_ID + " = ? " , new String[]{String.valueOf(event.getId())});
    }

    public void deleteEvent(long id) {
        String idd = String.valueOf(id);
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_EVENT, EVENT_ID + " = ? ", new String[]{idd});
        db.close();
    }

    public List<TableEvents> selectDayEvent(String dateStart ) {


        List<TableEvents> listEvents = selectAllInfoAboutEvent();
        List<TableEvents> events = new ArrayList<>();
        for (TableEvents event :listEvents) {

            try {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                Date dtStart = sdf.parse(dateStart);
                Date dtEvent = sdf.parse(event.getDateFrom());
                if (dtStart.equals(dtEvent)){
                    events.add(event);
                }

            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        return events;
    }

   public Date getSysdate() {
        Calendar now = Calendar.getInstance();
        int year = now.get(Calendar.YEAR);
        int month = now.get(Calendar.MONTH) + 1;
        int day = now.get(Calendar.DAY_OF_MONTH);
        return now.getTime();

    }

}