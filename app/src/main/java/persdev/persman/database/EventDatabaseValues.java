package persdev.persman.database;

import android.provider.BaseColumns;

/**
 * Created by shoorie on 18.03.16.
 */
public final class EventDatabaseValues {
    private EventDatabaseValues() {
    }

    public static final class Events implements BaseColumns {
        private Events() {
        }

        public static final String TABLE_EVENT = "events";
        public static final String EVENT_ID = "event_id";
        public static final String EVENT_NAME = "event_name";
        public static final String DATE_FROM = "date_from";
        public static final String DATE_TO = "date_to";
        public static final String FLAG = "flag";
        public static final String LATITUDE = "latitude";
        public static final String LONGITUDE = "longitude";
        public static final String ALARM_DATE = "alarm_date";
        public static final String CATEGORY = "category";
        public static final String CITY = "city";
    }
}

