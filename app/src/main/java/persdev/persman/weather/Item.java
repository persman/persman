package persdev.persman.weather;

import org.json.JSONObject;

/**
 * Created by shoorie on 18.03.16.
 */
public class Item implements JSONPopulator {

    private Condition condition;

    @Override
    public void populate(JSONObject obj) {
        condition = new Condition();
        condition.populate(obj.optJSONObject("condition"));
    }

    public Condition getCondition() {
        return condition;
    }

}
