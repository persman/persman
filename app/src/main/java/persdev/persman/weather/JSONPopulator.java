package persdev.persman.weather;

import org.json.JSONObject;

/**
 * Created by shoorie on 18.03.16.
 */
public interface JSONPopulator {
    void populate(JSONObject obj);
}
