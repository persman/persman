package persdev.persman.weather;

import org.json.JSONObject;

/**
 * Created by shoorie on 19.03.16.
 */
public class Units implements JSONPopulator {

    String temperature;

    @Override
    public void populate(JSONObject obj) {
        temperature = obj.optString("temperature");
    }

    public String getTemperature() {
        return temperature;
    }
}
