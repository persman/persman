package persdev.persman.weather;

import org.json.JSONObject;

/**
 * Created by shoorie on 18.03.16.
 */
public class Condition implements JSONPopulator{

    private String description;
    private int temperature;

    @Override
    public void populate(JSONObject obj) {
        description = obj.optString("text");
        temperature = obj.optInt("temp");
    }

    public int getTemperature() {
        return temperature;
    }

    public String getDescription() {
        return description;
    }
}
