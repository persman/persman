package persdev.persman.weather;

import org.json.JSONObject;

/**
 * Created by shoorie on 18.03.16.
 */
public class Channel implements JSONPopulator {

    private Item item;
    private Units units;

    @Override
    public void populate(JSONObject obj) {
        item = new Item();
        item.populate(obj.optJSONObject("item"));

        units = new Units();
        units.populate(obj.optJSONObject("units"));
    }

    public Item getItem() {
        return item;
    }

    public Units getUnits() {
        return units;
    }
}
