package persdev.persman.list;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.List;

import persdev.persman.R;
import persdev.persman.database.DatabaseManagement;
import persdev.persman.models.TableEvents;

/**
 * Created by shoorie on 19.03.16.
 */
public class CustomList extends BaseAdapter {

    Context context;
    List<TableEvents> data;
    private static LayoutInflater inflater = null;
    private String currentDate;
    public CustomList(Context context, List<TableEvents> data ,String currentDate) {
        this.context = context;
        this.data = data;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.currentDate = currentDate;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }



    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        View vi = convertView;
        if(vi == null) {
            vi = inflater.inflate(R.layout.tablevents_list_view_item_template, parent, false);
        }
        ImageView deleteButton  = (ImageView) vi.findViewById(R.id.delete);
        TextView eventNameTextView = (TextView) vi.findViewById(R.id.eventNameTextView);
        TextView eventTimeTextView = (TextView) vi.findViewById(R.id.eventTimeTextView);

        eventNameTextView.setText(data.get(position).getName());
        String[] s = data.get(position).getDateFrom().split(" ");
        vi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("dasdas", "" + position);
                AlertDialog.Builder alert = new AlertDialog.Builder(context);

                final TextView eventText = new TextView(context);
                eventText.setTextColor(Color.BLACK);
                eventText.setText("Nazwa wydarzenia: " + data.get(position).getName());
                eventText.setTextSize(16);

                final TextView category = new TextView(context);
                category.setTextColor(Color.BLACK);
                category.setText("Kategoria: " + data.get(position).getCategory());
                category.setTextSize(16);

                final TextView dateFrom = new TextView(context);
                dateFrom.setTextColor(Color.BLACK);
                dateFrom.setText("Data rozpoczęcia: " + data.get(position).getDateFrom());
                dateFrom.setTextSize(16);

                final TextView dateTo = new TextView(context);
                dateTo.setTextColor(Color.BLACK);
                dateTo.setText("Data zakończenia: " + data.get(position).getDateTo());
                dateTo.setTextSize(16);

                final TextView city = new TextView(context);
                city.setTextColor(Color.BLACK);
                city.setText("Miasto: " + data.get(position).getCity());
                city.setTextSize(16);

                LinearLayout layout = new LinearLayout(context);
                layout.setOrientation(LinearLayout.VERTICAL);
                layout.addView(eventText);
                layout.addView(category);
                layout.addView(dateFrom);
                layout.addView(dateTo);
                layout.addView(city);

                //     alert.setMessage("Szczegóły wydarzenia");
                alert.setTitle("Szczegóły wydarzenia");
                alert.setView(layout);
                alert.setPositiveButton("Powrót", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                alert.show();
            }
        });

        eventTimeTextView.setText(s[1]);

        deleteButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                DatabaseManagement db = new DatabaseManagement(context);
                db.deleteEvent(data.get(position).getId());
                ((ListView) parent.findViewById(R.id.list)).setAdapter(new CustomList(context, db.selectDayEvent(currentDate),currentDate));
            }
        });


        CheckBox flagSwitch = (CheckBox) vi.findViewById(R.id.flag);
        if(data.get(position).getFlag() == 1){
            flagSwitch.setChecked(true);
        } else {
            flagSwitch.setChecked(false);
        }
        flagSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                DatabaseManagement db = new DatabaseManagement(context);
                if (isChecked) {
                    data.get(position).setFlag(1);
                    db.modifyEvent(data.get(position));
                } else {
                    data.get(position).setFlag(0);
                    db.modifyEvent(data.get(position));
                }
            }
        });

        return vi;
    }


}
